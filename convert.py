#!/usr/bin/env python
import subprocess as sub
import sys

if len(sys.argv) != 2:
    print('Missing argument: output filename')
    sys.exit(-1)

# Grab text from clipboard
# Test if xclip is available
proc = sub.run(['xclip', '-h'], capture_output=True)
if proc.returncode != 0:
    print('This program uses `xclip`. Please install and rerun.')
    sys.exit(-2)

# Get the text from the clipboard
proc = sub.run(['xclip', '-o', '-selection', 'clipboard'], capture_output=True, text=True)
f = open(sys.argv[1], 'w')

card = ''
for line in proc.stdout.split('\n'):
    if '\t' in line:
        # There is a tab means that it is the start of a new card
        if card: f.write(card + '\n')
        card = line
    else:
        # No tab means that we append to the existing card
        card += '<br>' + line

# Write the remaining card if needed
if card: f.write(card + '\n')

f.close()
