# Quizlet to Anki format converter

I have found it annoying that Quizlet does not distinguish between newlines
(and Anki does) in exports. This has lead to problems in the export/import
process, where my Quizlet cards that have newlines aren't imported correctly
into Anki. This script solves this issue.

## Pre-requisites

- `xclip`

## How it works

This script gets your clipboard from command `xclip -o -selection clipboard`
and goes through it, line by line. Whenever it sees that a line has a tab, it
assumes that it is the start of a card. Whenever it sees that a line does not
have a tab, it assumes that it is a continuation of the previous card, and adds
it (along with HTML line break) to the previous card.

Whenever it thinks it has come to the end of a card (i.e. seeing that the next
line has a tab in it), it would write the aggregated card (along with a
newline) into the specified file.

## How to use

The script assumes that you have just freshly copied the Quizlet export onto
clipboard (xclip clipboard for most firefox users).

```sh
python convert.py a.out
```

Which would place your formatted selection into file `a.out`.

To see the amount of cards that this has formatted, you simply do `wc -l
a.out`, which counts the number of lines (which will be the number of cards
imported into Anki).
